let posts = [];
let count = 1;

// adding post data
document.querySelector("#form-add-post").addEventListener("submit", (e) =>
{
    // prevents the page from reloading after a button has been clicked/triggered
    e.preventDefault();

    posts.push({
        id: count,
        title: document.querySelector("#txt-title").value,
        body: document.querySelector("#txt-body").value
    })

    count++;

    showPosts(posts);
    alert("Post created.");
})

const showPosts = (posts) =>
{
    let postEntries = "";

    //adds information from the frontend to our mock database (posts[])
    posts.forEach(post => 
    {
        postEntries += `
            <div id = "post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onclick="editPost(${post.id})">Edit</button>
                <button onclick="deletePost(${post.id})">Delete</button>
            </div>
        `
    });
    // the div duplicates the objects inside the postEntries
    document.querySelector("#div-post-entries").innerHTML = postEntries;
}

const editPost = (postId) =>
{
    let post = posts.find(post => post.id === postId);
    document.querySelector("#txt-edit-id").value = post.id;
    document.querySelector("#txt-edit-title").value = post.title;
    document.querySelector("#txt-edit-body").value = post.body;

}

// other way (faster) - c/o sir M
/* const editPost = (postId) =>
{
    let title = document.querySelector(`#post-title-${postId}`).innerHTML;
    let body = document.querySelector(`#post-body-${postId}`).innerHTML;

    document.querySelector("#txt-edit-id").value = postId;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;
} */

// Update post using for loop - c/o sir M
/* document.querySelector("#form-edit-post").addEventListener("submit", (e) =>
{
    e.preventDefault();

    for (let i = 0; i < posts.length; i++) {
        if (posts[i].id.toString() === document.querySelector("#txt-edit-id").value) 
        {
            posts[i].title = document.querySelector("#txt-edit-title").value;
            posts[i].body = document.querySelector("#txt-edit-body").value;

            showPosts(posts);
            alert("Post updated.");

            break;
        }
    }
}); */

// Update post using findIndex
document.querySelector("#form-edit-post").addEventListener("submit", (e) =>
{
    e.preventDefault();
    let postId = document.querySelector("#txt-edit-id").value;
    let index = posts.findIndex(post => `${post.id}` === postId);
    posts[index].title = document.querySelector("#txt-edit-title").value;
    posts[index].body = document.querySelector("#txt-edit-body").value;

    showPosts(posts);
    alert("Post updated.");
});

// DELETE POST
const deletePost = (postId) =>
{
    let index = posts.findIndex(post => post.id === postId);
    posts.splice(index, 1);

    showPosts(posts);
}



